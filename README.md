# Agile assessment

WORK IN PROGRESS

* [Overview](#overview)
* [Agile values](#agile-values)
* [Agile values - exercise idea](#agile-values-exercise-idea)
* [Agile manifesto](#agile-manifesto)
* [Agile manifesto - exercise idea](#agile-manifesto-exercise-idea)
* [Lenses](#lenses)
* [Lenses - exercise idea](#lenses-exercise-idea)
* [Wordbook](#wordbook)


<h2><a name="overview">Overview</a></h2>

Agile has many meanings to many people. We like to start with the Wikipedia page about [Agile management](https://en.wikipedia.org/wiki/Agile_management).

Agile management, or agile process management, or simply agile, refers to an iterative, incremental method of managing the design and build activities of engineering, information technology and other business areas.

* The aim is to provide new product or service development in a highly flexible and interactive manner.

* It requires capable individuals from the relevant business, openness to consistent customer input, and management openness to non-hierarchical forms of leadership


<h2><a name="agile-values">Agile values</a></h2>

These come from the [agile manifesto](http://agilemanifesto.org/)

We are uncovering better ways of developing software by doing it and helping others do it.

Through this work we have come to value:

* Individuals and interactions over processes and tools

* Working software over comprehensive documentation

* Customer collaboration over contract negotiation

* Responding to change over following a plan

That is, while there is value in the items on the right, we value the items on the left more.


<h2><a name="agile-values-exercise-idea">Agile values - exercise idea</a></h2>

The exercise idea: compare the agile values to where the organizations/groups/individuals are, and want to be.

* Setup: show people the agile values as four left-to right lines. The lines can be stacked, or if there's enough space, then end-to-end.

* Ask each person to choose where they are on the line now. For example, put a stickie dot on the line.

* Ask each person to choose where they realistically want aim to be on the line on a specific future date, such as one year from now.

* Ask each person to comment on items they know about that fit along the lines.


<h2><a name="exercise-agile-manifesto">Agile manifesto</a></h2>

Agile maifesto principles:

* Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.

* Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.

* Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.

* Business people and developers must work together daily throughout the project.

* Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.

* The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.

* Working software is the primary measure of progress.

* Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.

* Continuous attention to technical excellence and good design enhances agility.

* Simplicity--the art of maximizing the amount of work not done--is essential.

* The best architectures, requirements, and designs emerge from self-organizing teams.

* At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.


<h2><a name="exercise-agile-manifesto">Agile manifesto - exercise idea</a></h2>

The exercise idea: compare the agile manifest list of items with the organizations/groups/individuals are, and want to be.

* Setup: show people the agile manifesto list of items. The list can be stacked, or if there's enough space, then in columsn.

* We suggest rating from 1 to 5 (worst to best).

* We suggest having people comment. For example, comment via a web form, or chat message, or stickies.


<h2><a name="lenses">Lenses</a></h2>

We can use various "lenses" a.k.a. "perspectives" for assessment, comparisons, planning, and growth.

Strategic Balanced Scorecard:
* Create a a destination Statement.
* Cover four areas: financial, internal, external, learning.
* Define Objectives and Key Results (OKRs).
* Define Key Performance Indicators (KPIs) and measure them.

RACIO responsibility assigment matrix:
* Responsible
* Accountable
* Consultable
* Informable
* Omittable

TEAM interpersonal aims:
* Talk
* Evaluate
* Assist
* Motivate

FOCUS analytical aims:
* Frame
* Organize
* Collect
* Understand
* Synthesize

ADKAR model of change management:
* Awareness
* Desire
* Knowledge
* Ability
* Reinforcement

SMART objectives are:
* Specific
* Measurable
* Achievable (a.k.a. Attainable, Agreed)
* Relevant (a.k.a. Realistic, Responsible, Receivable)
* Timely (a.k.a. Time-scoped, Time-boxed, Time-bound)

Maturity model levels:
* 0. No capability
* 1. Initial capability
* 2. Under development; core processes managed; vertical alignment.
* 3. Defined; core processes integrated; horizontal alignment.
* 4. Managed; enabling processes integrated; total alignment.
* 5. Optimizing; processes are holistic.

Maturity model categories:
* People
* Processes
* Technology
* Controls
* Strategy

Value Stream Mapping (VSM):
* value-adding times a.k.a. the processes
* non-value-adding times a.k.a. the operations, waste, muda

DMADV project methodology, known as DFSS ("Design For Six Sigma"), features 5 phases:
* Define design goals that are consistent with customer demands and the enterprise strategy.
* Measure CTQs (characteristics that are Critical To Quality), capabilities, prrisks etc.
* Analyze to develop and design alternatives
* Design an improved alternative, best suited per analysis in the previous step
* Verify the design, set up pilot runs, implement the production process and hand it over to the process owner(s).

DMAIC:
* Define
* Measure
* Analyze
* Improve
* Control

DDICA:
* Design
* Develop
* Initialize
* Control
* Allocate

SIPOC:
* Supplier
* Input
* Process
* Output
* Customer

Program Evaluation and Review Technique (PERT):
* critical path
* lead time
* lag time
* float or slack

Domain Driven Design:
* [Ubiquitous Language](http://martinfowler.com/bliki/UbiquitousLanguage.html)
* [Bounded Context](http://martinfowler.com/bliki/BoundedContext.html)


<h2><a name="lenses-exercise-idea">Lenses - exercise idea</a></h2>

The exercise idea: compare the agile values to where the organizations/groups/individuals are, and want to be.

Some of the ways to do agile assessments are by using various kinds of lenses, also known as perspectives.

For example, have the team pick one or more of the lenses above, and explore how it relates to the current state and desired future state.


<h2><a name="wordbook">Wordbook</a></h2>

* [5 Whys](http://www.startuplessonslearned.com/2008/11/five-whys.html)
* [5 Whys and root cause analysis (RCA)](http://www.startuplessonslearned.com/2009/07/how-to-conduct-five-whys-root-cause.html)
* [Agile management](https://en.wikipedia.org/wiki/Agile_management)
* [Extreme project management](https://en.wikipedia.org/wiki/Extreme_project_management)
* [Strategic Balanced Scorecard](https://github.com/joelparkerhenderson/strategic_business_scorecard)
* [Program evauation and review technique (PERT)](https://en.wikipedia.org/wiki/Program_evaluation_and_review_technique)
* [Domain-driven design (DDD)](https://en.wikipedia.org/wiki/Domain-driven_design)
* [ADKAR change management model](https://www.prosci.com/adkar/adkar-model): Awareness, Desire, Knowledge, Ability, Reinforcement
* [Responsibility assignemtn matrix](https://en.wikipedia.org/wiki/Responsibility_assignment_matrix)
* [RACIO](https://en.wikipedia.org/wiki/Responsibility_assignment_matrix): Responsible, Accountable, Consultable, Informable, Omittable
* [SIPOC](https://en.wikipedia.org/wiki/SIPOC): a tool that summarizes using supplier, input, process, output, customer.
* [CTQ tree](https://en.wikipedia.org/wiki/CTQ_tree): ) are the key measurable characteristics of a product or process whose performance standards or specification limits must be met in order to satisfy the customer.
* [SMART critera](https://en.wikipedia.org/wiki/SMART_criteria): Specific, Measurable, Achievable, Relevant, Timely
* [TEAM interpersonal aims](https://github.com/joelparkerhenderson/team_focus): Talk, Evaluate, Assist, Motivate
* [FOCUS analytical aims](https://github.com/joelparkerhenderson/team_focus): Frame, Organize, Collect, Understand, Synthesize
* [SBS: Strategic Balanced Scorecard](https://github.com/joelparkerhenderson/strategic_balanced_scorecard)
* [MBO: management by objectives](https://en.wikipedia.org/wiki/Management_by_objectives)
* [programmer competency matrix](http://sijinjoseph.com/programmer-competency-matrix/)
* [Objective and Key Results (OKRs)](https://en.wikipedia.org/wiki/OKR)
* [Key Performance Indicator (KPI)](https://en.wikipedia.org/wiki/Performance_indicator)
* [Value stream mapping (VSM)](https://en.wikipedia.org/wiki/Value_stream_mappin)g
* [Quality filter mapping](https://en.wikipedia.org/wiki/Quality_filter_mapping)
* [Design for Six Sigma (DFSS)](https://en.wikipedia.org/wiki/Design_for_Six_Sigma)
* [Six Sigma](https://en.wikipedia.org/wiki/Six_Sigma)
* [DMAIC](https://en.wikipedia.org/wiki/DMAIC): Define, Measure, Analyze, Improve, Control


<h2><a name="see">See also</a></h2>

* [Metrics and Performance Measurement System for the Lean Enterprise by Professor Deborah Nightingale](https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-852j-integrating-the-lean-enterprise-fall-2005/lecture-notes/12_metrics.pdf)
